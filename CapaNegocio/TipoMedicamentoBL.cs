﻿using CapaEntidad;
using CapaDatos;
namespace CapaNegocio
{
    public class TipoMedicamentoBL
    {
        public List<TipoMedicamentoCLS> listarMedicamentos()
        {
            TipoMedicamentoDAT obj = new TipoMedicamentoDAT();
            return obj.listarMedicamentos();
        }
    }
}
