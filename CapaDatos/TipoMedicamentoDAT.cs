﻿using CapaEntidad;

namespace CapaDatos
{
    public class TipoMedicamentoDAT
    {
        public List<TipoMedicamentoCLS> listarMedicamentos()
        {
            List<TipoMedicamentoCLS> Lista = new List<TipoMedicamentoCLS> ();
            Lista.Add(new TipoMedicamentoCLS { 
                idtipomedicamento = 1,
                nombre = "Analgesico",
                descripcion = "Descripción 1 "
            });
            Lista.Add(new TipoMedicamentoCLS
            {
                idtipomedicamento = 1,
                nombre = "Anti Alergicos",
                descripcion = "Descripción 2"
            });

            return Lista;
        }
    }
}
