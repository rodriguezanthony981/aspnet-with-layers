﻿using Microsoft.AspNetCore.Mvc;
using CapaNegocio;
using CapaEntidad;

namespace NetCoreFirstAppWithLayers.Controllers
{
    public class TipoMedicamentoController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Inicio() {
            return View();
        }

        public IActionResult SinMenu()
        {
            return View();
        }

        //String
        public string Saludos()
        {
            return "Hello Friends";
        }

        public string SaludoNombre(string nombre)
        {
            return "Bienvenido " + nombre;
        }

        public string SaludoNombreApellido(string nombre, string apellido)
        {
            return "Bienvenido " + nombre + " " + apellido;
        }

        public int Number()
        {
            return 1;
        }

        public double NumeroDecimal()
        {
            return 12.5;
        }

        public List<TipoMedicamentoCLS> listarTipoMedicamento()
        {
            TipoMedicamentoBL objBL = new TipoMedicamentoBL();
            return objBL.listarMedicamentos();
        }
    }
}
